import pandas as pd
import numpy as np
from functions import get_z_score
from datetime import datetime
from dateutil.relativedelta import relativedelta

def to_features(df: pd.DataFrame):
    pass


# not tested
def z_score_normalized_flow(df: pd.DataFrame):
    ret = []
    mean = np.mean(df['Q.cms'])
    std_dev = np.std(df['Q.cms'])
    for index, row in df.iterrows():
        if row['Q.FLAG'] == 1:
            z = get_z_score(row['Q.cms'], mean, std_dev)
            ret.append(z)
        else:
            ret.append(np.nan)
    return ret

def midday(df: pd.DataFrame):
    ret = []
    for index, row in df.iterrows():
        time = row["Time"]
        hour, minute = map(int, time.split(":"))
        ret.append(abs(12-hour))
        #a = datetime.now().replace(hour=hour, minute=minute)
        #t1 = datetime.now().replace(hour=12, minute=0)
        #diff = t1-a
        #ret.append(diff.seconds/3600)

    return ret

def trend(df: pd.DataFrame, column="Q.cms"):
    df = df.reset_index(inplace=False)
    rolling = df[column].rolling(10, center=True)
    return rolling.apply(lambda d: d[5:].mean() - d[:5].mean())


def rolling_mean(df: pd.DataFrame, column="Q.cms", window_size="30D"):
    return df[column].rolling(window_size).mean().reset_index()[column]
