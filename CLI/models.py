import numpy as np

from sklearn.linear_model import LogisticRegression
from sklearn import metrics
import pickle


class Model:
    def save(self, filename):
        raise NotImplementedError()

    def load(self, filename):
        raise NotImplementedError()

    def train_sequence(self, train_x, train_y):
        raise NotImplementedError()

    def train(self, train_x, train_y):
        raise NotImplementedError()

    def predict(self, x):
        raise NotImplementedError()

    def evaluate(self, test_x, test_y):
        raise NotImplementedError()


class LogReg(Model):
    def __init__(self, num_features):
        '''
        - giving higher weights to the minority class (which is the isstorm label)
        - giving lower weights to the majority class (which is the not isstorm label)
        - therefore the model has a penalty of predicting the minority class 99 times more severe
          then when predicting the majority class
        '''
        w = {0:1 , 1:1}
        self.LR_model = LogisticRegression(solver='lbfgs', class_weight = w)
        #self.LR_model = LogisticRegression(solver='lbfgs')

    def evaluate(self, test_x, test_y):
        return self.LR_model.score(test_x, test_y)

    def train(self, train_x, train_y):
        self.LR_model.fit(train_x, train_y)

################################################################################
#don't need this function since I am no longer using the load_balance_data function
################################################################################
    '''
    def train_sequence(self, train_x, train_y):
        train_x = np.concatenate(train_x)
        train_y = np.concatenate(train_y)
        self.train(train_x, train_y)
    '''

    def predict(self, x):
        return self.LR_model.predict(x)

    def save(self, filename):
        pickle.dump(self.LR_model, open(filename, 'wb'))

    def load(self, filename):
        self.LR_model = pickle.load(open(filename, 'rb'))


def expand_if_needed(x):
    return x if len(x.shape) == 3 else np.expand_dims(x, 0)


class CNN(Model):
    def __init__(self, num_features):
        import tensorflow.keras as K
        import tensorflow as tf
        self.tf = tf
        self.K = K
        self.model = self.K.models.Sequential([
            self.K.layers.Conv1D(64, 32, input_shape=(None, num_features), padding="same", activation="relu"),
            #self.K.layers.MaxPool1D(2, padding="same"),
            self.K.layers.Conv1D(24, 12, padding="same", activation="relu"),
            #self.K.layers.MaxPool1D(2, padding="same"),
            self.K.layers.Conv1D(12, 6, padding="same", activation="relu"),
            #self.K.layers.MaxPool1D(2, padding="same"),
            self.K.layers.Conv1D(1, 1, activation="sigmoid", padding="same")
        ])

        self.loss_func = self.K.losses.BinaryCrossentropy()
        self.optimizer = self.K.optimizers.Adam()
        self.model.compile(optimizer=self.optimizer, loss=self.loss, metrics=["acc"])

    def loss(self, y_true, y_pred):
        return self.loss_func(y_true, y_pred)

    def save(self, filename):
        self.model.save(filename)

    def load(self, filename):
        self.model = self.K.models.load_model(filename, compile=False)
        self.model.compile(optimizer=self.optimizer, loss=self.loss, metrics=["acc"])

    def train(self, train_x, train_y):
        train_x = expand_if_needed(train_x)
        train_y = np.expand_dims(expand_if_needed(train_y), axis=2)
        self.model.fit(train_x, train_y, epochs=100)

################################################################################
#don't need this function since I am no longer using the load_balance_data function
################################################################################
    '''
    def train_sequence(self, train_x, train_y):
        count = 0
        for (x, y) in zip(train_x, train_y):
            if len(x) >= 10:
                count += 1
                self.train(x, y)
        print("COUNT: ", count)
    '''

    def predict(self, x):
        x = expand_if_needed(x)
        return np.round(self.model(x)[0])

    def evaluate(self, test_x, test_y):
        test_y = np.expand_dims(expand_if_needed(test_y), axis=2)
        bin = self.K.metrics.BinaryAccuracy()
        bin.update_state(test_y, self.predict(test_x))
        return bin.result().numpy()


algorithms = {"logreg": LogReg, "cnn": CNN}
