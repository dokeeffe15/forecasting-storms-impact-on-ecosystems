from functions import read_observs
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn import metrics
import pandas as pd
import numpy as np
from features import z_score_normalized_flow, trend, rolling_mean, midday
from models import CNN

#from imblearn.over_sampling import RandomOverSampler
from imblearn.over_sampling import SMOTE
from imblearn.under_sampling import RandomUnderSampler
#from collections import Counter
from imblearn.pipeline import Pipeline

import pickle

################################################################################
#load_balance_data function not working the way it is supposed to
################################################################################
'''
def load_balance_data(flow_csv, precip_csv, flow_rate_column):
    data = load_raw_data(flow_csv, precip_csv, flow_rate_column).reset_index()

    # group data by storm
    data["cumsum"] = (1 - data["is_storm"]).cumsum()
    grouped_data = data.groupby("cumsum")

    min_storm_length = 2
    filtered_groups = grouped_data.filter(lambda x: len(x) > min_storm_length).groupby("cumsum")

    # Expand the storm regions to include about equal non-storm points
    def expand_spans(df):
        storm_len = len(df) - 1
        start = df.index[1]
        end = df.index[-1] + 1
        count1s = storm_len - 1
        while count1s != 0:
            if start - 1 > 0:
                start -= 1
                added_val = data["is_storm"].loc[start]
                count1s += added_val
                count1s -= 1 - added_val

            if end + 1 < len(data.index) and count1s != 0:
                end += 1
                added_val = data["is_storm"].loc[end]
                count1s += added_val
                count1s -= 1 - added_val
        return data.loc[start:end]

    final_output = filtered_groups.apply(expand_spans)

    # Extract the sequences as numpy arrays
    def extract_sequences(multidexed_sequences):
        actual_indexes = set()
        for r in multidexed_sequences.index:
            actual_indexes.add(int(r[0]))

        sequences = []
        for i in actual_indexes:
            sequences.append(multidexed_sequences.loc[i].to_numpy())
        return sequences

    multidexed_features = final_output.drop(columns=["index", "cumsum", "is_storm"])
    multidexed_labels = final_output["is_storm"]

    feature_seqs = extract_sequences(multidexed_features)
    label_seqs = extract_sequences(multidexed_labels)

    return feature_seqs, label_seqs
'''

def load_raw_data(flow_csv, precip_csv, flow_rate_column):
    if flow_rate_column is None:
        flow_rate_column = "Q.cms"

    data = read_observs(flow_csv)

    labels = pd.read_csv(precip_csv)

    data = data.reset_index()

    data["DateTime"] = pd.to_datetime(data["DateTime"], format='%Y-%m-%d %H:%M:%S')

    labels["DateTime"] = pd.to_datetime(labels["DateTime"], format='%Y-%m-%d %H:%M:%S')

    labels_and_data = pd.merge_asof(data, labels, on = 'DateTime', by = 'DateTime', allow_exact_matches = True )

    labels_and_data = labels_and_data.set_index('DateTime')

    labels = labels_and_data.reset_index()["is_storm"]

    r = rolling_mean(labels_and_data)
    t = trend(labels_and_data)
    z = z_score_normalized_flow(labels_and_data)
    m = midday(labels_and_data)

    frame = {"z-score": z, "trend": t, "rolling_mean": r, "flow": labels_and_data.reset_index()[flow_rate_column], "midday": m,
             "DateTime": labels_and_data.reset_index()["DateTime"], "is_storm": labels}

    features = pd.DataFrame(frame)

    features = features[features['z-score'].notna()]
    features = features[features['trend'].notna()]
    features = features[features['rolling_mean'].notna()]
    features = features[features['flow'].notna()]
    features = features[features['midday'].notna()]
    features = features[features['is_storm'].notna()]
    features = features[features['DateTime'].notna()]

    return features


def load_data(flow_csv, precip_csv, flow_rate_column):
    features = load_raw_data(flow_csv, precip_csv, flow_rate_column)

    labels = features["is_storm"]

    features = features.drop("is_storm", 1)
    features = features.drop("DateTime", 1)

    return features.to_numpy(), labels.to_numpy()


def predict_flow(flow_csv, out_csv, model_type, model_file, flow_csv_name):

    data = read_observs(flow_csv)

    data = data.reset_index()

    data["DateTime"] = pd.to_datetime(data["DateTime"], format='%Y-%m-%d %H:%M:%S')
    prev_data_datetime = data["DateTime"]

    data.set_index("DateTime", inplace=True)

    r = rolling_mean(data)
    t = trend(data)
    z = z_score_normalized_flow(data)
    m = midday(data)

    frame = {"z-score": z, "trend": t, "rolling_mean": r, "flow": data.reset_index()["Q.cms"], "midday": m,
             "date": data.reset_index()["DateTime"]}
    features = pd.DataFrame(frame)

    features = features[features['z-score'].notna()]

    features = features[features['trend'].notna()]

    features = features[features['flow'].notna()]

    valid_dates = features["date"]
    features = features.drop("date", 1)

    features_predx = features.to_numpy()

    num_features = features_predx.shape[1]

    model = model_type(num_features)
    model.load(model_file)
    predictions = model.predict(features_predx)

    final_predictions = []

    # idk how 2 numpy :(
    valid_date_index = 0
    all_date_index = 0

    while valid_date_index < len(valid_dates) and all_date_index < len(prev_data_datetime):
        # if not a valid date
        if valid_dates.iloc[valid_date_index] != prev_data_datetime[all_date_index]:
            final_predictions.append(0)
        else:
            final_predictions.append(predictions[valid_date_index])
            valid_date_index += 1
        all_date_index += 1

    #4 off for some reason - i think because of the rolling mean feature but not sure
    #also predicting all 0's when no dates passed in i think(was doing that before)
    for i in range(4):
        final_predictions.append(0)

    output = pd.DataFrame()

    #output["DateTime"] = valid_dates
    #output["isStorm"] = predictions

    output = pd.read_csv(flow_csv_name, skip_blank_lines=True)
    output.dropna(how="all", inplace=True)

    output["isStorm"] = final_predictions
    output.to_csv(out_csv, index=False)


def train_model(flow_csv, precip_csv, model_type, model_file, flow_rate_column):

    # x_train_balance, y_train_balance = load_balance_data(flow_csv, precip_csv, flow_rate_column)
    # flow_csv.seek(0)
    x_train, y_train = load_data(flow_csv, precip_csv, flow_rate_column)

    # x_train_1, x_train_2 = np.array_split(x_train, 2)
    # y_train_1, y_train_2 = np.array_split(y_train, 2)

    #ros = RandomOverSampler(sampling_strategy="minority")
    #x_train, y_train = ros.fit_resample(x_train, y_train)
    #x_train_2, y_train_2 = ros.fit_resample(x_train_2, y_train_2)

    oversample = SMOTE()
    x_train, y_train = oversample.fit_resample(x_train, y_train)

    num_features = x_train.shape[1]
    model = model_type(num_features)

    model.train(x_train, y_train)

    predictions = model.predict(x_train)
    print(predictions)

    score = model.evaluate(x_train, y_train)
    print("Accuracy is", score)

    cm = metrics.confusion_matrix(y_train, predictions)
    print(cm)

    macro_f1 = metrics.f1_score(y_train, predictions, average='macro')
    print("macro averaged f1 is:", macro_f1)

    true_negatives = cm[0][0]
    false_negatives = cm[1][0]
    true_positives = cm[1][1]
    false_positives = cm[0][1]

    #Precision = TruePositives / (TruePositives + FalsePositives)
    precision = true_positives / (true_positives + false_positives)
    print("precision is: ", precision)
    #Recall = TruePositives / (TruePositives + FalseNegatives)
    recall = true_positives / (true_positives + false_negatives)
    print("recall is: ", recall)
    #F1-measure = 2 * ((precision * recall) / (precision + recall))
    F1_Score = 2 * ((precision * recall) / (precision + recall))
    print("F1-measure is: ", F1_Score)

    # save model
    model.save(model_file)


def test_model(flow_csv, precip_csv, model_type, model_file, flow_rate_column):

    features = load_raw_data(flow_csv, precip_csv, flow_rate_column)


    ############################################################################
    # below chunk of code picked out storms that had high precipitation values.

    # testing my model with these datapoints is helpful because since I know all 
    # classified points should be true positives, I can rework my ground truth
    # paramater of rolling window size to get the parameter that comes up with the
    # most true positives. I can then use this parameter on the untouched raw test data
    ############################################################################

    '''

    df = pd.read_csv("LabeledByPrecipitation.csv")

    precipitation_datetime = df['DateTime']

    datetimes = precipitation_datetime[df["Val"] > 0]

    datetimes = datetimes.to_frame()

    temp = datetimes['DateTime'].tolist()

    features = features[features['DateTime'].isin(temp)]

    '''

    x_test = features.iloc[:,0:5]

    y_test = features.iloc[:,6]

    num_features = x_test.shape[1]

    model = model_type(num_features)
    model.load(model_file)

    predictions = model.predict(x_test)
    print(predictions)

    score = model.evaluate(x_test, y_test)
    print("Accuracy is: ", score)

    cm = metrics.confusion_matrix(y_test, predictions)
    print(cm)

    macro_f1 = metrics.f1_score(y_test, predictions, average='macro')
    print("macro averaged f1 is:", macro_f1)

    true_negatives = cm[0][0]
    false_negatives = cm[1][0]
    true_positives = cm[1][1]
    false_positives = cm[0][1]

    #Precision = TruePositives / (TruePositives + FalsePositives)
    precision = true_positives / (true_positives + false_positives)
    print("precision is: ", precision)
    #Recall = TruePositives / (TruePositives + FalseNegatives)
    recall = true_positives / (true_positives + false_negatives)
    print("recall is: ", recall)
    #F1-measure = 2 * ((precision * recall) / (precision + recall))
    F1_Score = 2 * ((precision * recall) / (precision + recall))
    print("F1-measure is: ", F1_Score)

    plt.figure(figsize=(9, 9))
    sns.heatmap(cm, annot=True, fmt=".3f", linewidths=.5, square=True, cmap='Blues');
    plt.ylabel('Actual label');
    plt.xlabel('Predicted label');
    plt.title("Predicted Scores of Model", size=15);
    plt.savefig("output.png")
