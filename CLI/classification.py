from visualizations import normalize_columns, date_to_sec, normalize_datetime, visualizations, make_solute_plots
import matplotlib.pyplot as plt
import numpy as np

def get_trend(arr):
    numpy_arr = arr
    del numpy_arr["DateTime"]
    del numpy_arr["is_storm"]
    cov = numpy_arr.cov()
    print(numpy_arr.cov())
    cov_flow_vs_time = cov["flow"]["NormTime"]
    cov_sol_vs_time = cov["solute"]["NormTime"]
    print(cov_flow_vs_time)

    if((cov_flow_vs_time > 0 and cov_sol_vs_time < 0) or (cov_flow_vs_time < 0 and cov_sol_vs_time > 0)):
        return "clockwise"

    elif((cov_flow_vs_time > 0 and cov_sol_vs_time > 0) or (cov_flow_vs_time < 0 and cov_sol_vs_time < 0)):
        return "counterclockwise"

    else:
        print("SOMETHIGN WENT WRONG")
        return ":("

    #print(arr)
    #print(type(arr))


def classify(storm_df):
    print(type(input))
    print("test")
    start_date = storm_df.iloc[1]["DateTime"]
    end_date = storm_df.iloc[-1]["DateTime"]
    df = normalize_columns(storm_df, start_date, end_date)

    #maybe make this 3?
    clock = 0
    cclock = 0
    for arr in np.array_split(df, 5):
        print("seperator")
        classif = get_trend(arr)
        if(classif == "clockwise"):
            clock += 1
        if(classif == "counterclockwise"):
            cclock += 1

    if(clock > cclock):
        return "clockwise"
    if (cclock > clock):
        return "counterclockwise"



def classify_plots(flowcsv, labelled_precip_csv):
    with open(flowcsv) as flow_csv:
      with open(labelled_precip_csv) as precip_csv:
          data = visualizations(flow_csv, precip_csv).reset_index()

          classified = classify(data)
          plt.title(classified)
          make_solute_plots(data)
          plt.xlabel("flow rate")
          plt.ylabel("solute")
          # plt.title("flow rate vs solute; normalized time from light red->dark red")
          i = 1
          plt.savefig("output" + str(i) + ".png")
          i = i + 1
          plt.clf()



          #plt.xlabel("flow rate")
          #plt.ylabel("solute")
          #plt.title("flow rate vs solute; normalized time from light red->dark red")
          #plt.savefig("output2.png")

        #make_plots(flow_csv, precip_csv)

        #visualizations(flow_csv, precip_csv)
