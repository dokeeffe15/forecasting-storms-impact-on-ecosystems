import time 

from MLfunctions import test_model, predict_flow, train_model
from models import algorithms
import argparse
from functions import label_dumb_storms
import os
import sys
from functions import get_start_date, get_end_date
from visualizations import make_main_plots
from classification import classify_plots

precip_name = "LabeledByPrecipitation.csv"


class Parse(object):

    def __init__(self):

        parser = argparse.ArgumentParser(
            description='Get ML learning type and training data filenames. There are 3 modes, train, test, and prediction.',
            usage='''main.py <command> [<args>]

        Commands to use:
           train     Performs training on flow data and outputs model in working directory. Arguments are precipitation csv, flow rate csv, and algorithm to use.
           test    Performs testing on flow data, and outputs a confusion matrix. Arguments are precipitation csv, and flow rate csv.
           predict  Predicts using the model used in train(uses testing file in directory). Argument is flow rate data and output csv name.
           visualize Creates flow vs solute vs time plots. Uses labelled precipitation data for storm data right now. TODO need to use actual ML data as well as output multiple plots.
           classify Creates flow vs solute vs time plots and classifies them as counterclockwise or clockwise. Outputs PNGs with classification as title
        ''')
        parser.add_argument('command', help='Subcommand to run')
        args = parser.parse_args(sys.argv[1:2])
        if not hasattr(self, args.command):
            print("Unrecognized command")
            parser.print_help()
            exit(1)

        getattr(self, args.command)()

    def train(self):
        parser = argparse.ArgumentParser(
            description='Performs training on flow data and outputs model in working directory. Arguments are precipitation csv, flow rate csv, and algorithm to use')
        parser.add_argument('-f', dest='flow', required=True,
                            help=".csv file that contains flow rate from https://ddc.unh.edu/")
        parser.add_argument('-p', dest='precip', required=True,
                            help=".csv file that contains precipitation from https://www.ncei.noaa.gov/data/coop-hourly-precipitation/v2/")
        parser.add_argument('-l', dest='alg', required=True,
                            help="Machine learning algorithm you would like to use. (Current options are: logreg, cnn)")
        parser.add_argument('-m', dest='model_file', required=True,
                            help="File to save the model to.")
        parser.add_argument('-s', dest='start_date',
                            help="Start date of flow data you would like to use. Example format: 2014-8-30. Will use all flow data if not specified")
        parser.add_argument('-e', dest='end_date',
                            help="End date of flow data you would like to use. Example format: 2014-8-30. Will use all flow data if not specified")
        parser.add_argument('-q', dest='flow_rate_column',
                            help="If the flow rate .csv file's column for flow rate is named anything other than Q.cms, use this parameter to specify what the column is called in the .csv")
        args = parser.parse_args(sys.argv[2:])

        with open(args.flow) as flow, open(args.precip) as precip:
            if os.path.exists(precip_name):
                os.remove(precip_name)

            with open(args.flow) as flow_date:
                start_date = get_start_date(flow_date, args.start_date)

            with open(args.flow) as flow_date:
                end_date = get_end_date(flow_date, args.end_date)

            label_dumb_storms(precip, precip_name, start_date, end_date)
            train_model(flow, precip_name, algorithms[args.alg], args.model_file, args.flow_rate_column)

    def test(self):
        parser = argparse.ArgumentParser(
            description='Performs testing on flow data, and outputs a confusion matrix. Arguments are precipitation csv, and flow rate csv.')
        parser.add_argument('-f', dest='flow', required=True,
                            help=".csv file that contains flow rate from https://ddc.unh.edu/")
        parser.add_argument('-p', dest='precip', required=True,
                            help=".csv file that contains precipitation from https://www.ncei.noaa.gov/data/coop-hourly-precipitation/v2/")
        parser.add_argument('-m', dest='model_file', required=True,
                            help="File to load the model from.")
        parser.add_argument('-l', dest='alg', required=True,
                            help="Machine learning algorithm used by the file you're loading. (Current options are: logreg, cnn)")
        parser.add_argument('-s', dest='start_date',
                            help="Start date of flow data you would like to use. Example format: 2014-8-30. Will use all flow data if not specified")
        parser.add_argument('-e', dest='end_date',
                            help="End date of flow data you would like to use. Example format: 2014-8-30. Will use all flow data if not specified")
        parser.add_argument('-q', dest='flow_rate_column',
                            help="If the flow rate .csv file's column for flow rate is named anything other than Q.cms, use this parameter to specify what the column is called in the .csv")
        args = parser.parse_args(sys.argv[2:])
        with open(args.flow) as flow, open(args.precip) as precip:
            if os.path.exists(precip_name):
                os.remove(precip_name)

            start_date = get_start_date(flow, args.start_date)
            end_date = get_end_date(flow, args.end_date)

            label_dumb_storms(precip, precip_name, start_date, end_date)
            test_model(flow, precip_name, algorithms[args.alg], args.model_file, args.flow_rate_column)

    def predict(self):
        parser = argparse.ArgumentParser(
            description='Predicts using the model used in train(uses testing file in directory). Argument is flow rate data and output csv name.')
        parser.add_argument('-f', dest='flow', required=True,
                            help=".csv file that contains flow rate from https://ddc.unh.edu/")
        parser.add_argument('-m', dest='model_file', required=True,
                            help="File to load the model from.")
        parser.add_argument('-l', dest='alg', required=True,
                            help="Machine learning algorithm used by the file you're loading. (Current options are: logreg, cnn)")
        parser.add_argument('-o', dest='output', required=True,
                            help="name of .csv file you would like the output to be. example: output.csv")

        args = parser.parse_args(sys.argv[2:])
        with open(args.flow) as flow:
            if os.path.exists(args.output):
                os.remove(args.output)
            predict_flow(flow, args.output, algorithms[args.alg], args.model_file, args.flow)

    def visualize(self):
        parser = argparse.ArgumentParser(
            description='Creates flow vs solute vs time plots, saved as output PNGs. Uses labelled precipitation data for storm data right now. TODO need to use actual data.')
        parser.add_argument('-f', dest='flow', required=True,
                            help=".csv file that contains flow rate from https://ddc.unh.edu/")
        parser.add_argument('-l', dest='lab_storm', required=True,
                            help="Name of labeled storm data")

        args = parser.parse_args(sys.argv[2:])
        make_main_plots(args.flow, args.lab_storm)

    def classify(self):
        parser = argparse.ArgumentParser(
            description='Creates flow vs solute vs time plots and classifies them as counterclockwise or clockwise. Outputs PNGs with classification as title')
        parser.add_argument('-f', dest='flow', required=True,
                            help=".csv file that contains flow rate from https://ddc.unh.edu/")
        parser.add_argument('-l', dest='lab_storm', required=True,
                            help="Name of labeled storm data")

        args = parser.parse_args(sys.argv[2:])
        classify_plots(args.flow, args.lab_storm)

if __name__ == '__main__':
    Parse()
