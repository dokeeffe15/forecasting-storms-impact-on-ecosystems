import matplotlib.pyplot as plt
import seaborn as sns
from sklearn import metrics
import pandas as pd
import numpy as np
from functions import read_observs
from sklearn import preprocessing



def visualizations(flow_csv, precip_csv):

    data = read_observs(flow_csv)

    labels = pd.read_csv(precip_csv)

    data = data.reset_index()

    data["DateTime"] = pd.to_datetime(data["DateTime"], format='%Y-%m-%d %H:%M:%S')

    labels["DateTime"] = pd.to_datetime(labels["DateTime"], format='%Y-%m-%d %H:%M:%S')

    labels = labels.set_index("DateTime")

    labels = labels.resample('15min').ffill() # upsampling each hour interval to 4, 15 minute intervals... forward filling each value with the current hours value

    data = data.set_index("DateTime")

    frame = {"DateTime": data.reset_index()["DateTime"], "flow": data.reset_index()["Q.cms"], "solute": data.reset_index()["SpCond.uScm.Hobo"],
    "is_storm": labels.reset_index()["is_storm"]}

    df = pd.DataFrame(frame)

    df = df[df['flow'].notna()]
    df = df[df['solute'].notna()]
    df = df[df['is_storm'].notna()]
    df = df[df['DateTime'].notna()]

    return df


def normalize_columns(df, start_date, end_date):


    df = df[(df['DateTime'] > start_date) & (df['DateTime'] < end_date)]
    #print(df)
    datetime_df = df["DateTime"].reset_index(drop=True)
    #print(datetime_df)
    df = df[['flow', 'solute', 'is_storm']]
    x = df.values #returns a numpy array
    min_max_scaler = preprocessing.MinMaxScaler()
    x_scaled = min_max_scaler.fit_transform(x)
    df = pd.DataFrame(x_scaled)


    df = df.rename(columns={0: "flow", 1: "solute", 2: "is_storm"})
    df["DateTime"] = datetime_df
    df = normalize_datetime(df)
    #print(df)

    return df

def date_to_sec(date):
    return (date - pd.Timestamp("1970-01-01")) // pd.Timedelta('1s')

def normalize_datetime(df):
    normalized_list = []
    #max_date = df["flow"].max()
    max_date = df.iloc[df['flow'].argmax()]["DateTime"]
    max_date = date_to_sec(max_date)
    start_date = date_to_sec(df.iloc[0]["DateTime"])

    if start_date == max_date:
        max_date = date_to_sec(df.iloc[1]["DateTime"])

    #seconds difference from start_date - current_date / start_date - max_date
    for index, row in df.iterrows():
        curr_date = date_to_sec(row["DateTime"])
        normalized = abs(start_date - curr_date) / abs(start_date - max_date)
        normalized_list.append(normalized/2)
        if curr_date == max_date:
            break

    start_date = max_date
    max_date = df.iloc[-1]["DateTime"]
    max_date = date_to_sec(max_date)


    for index, row in df.iterrows():
        curr_date = date_to_sec(row["DateTime"])
        if start_date >= curr_date:
            continue
        normalized = abs(start_date - curr_date) / abs(start_date - max_date)
        normalized_list.append((normalized/2) + 0.5)
        if curr_date == max_date:
            break

    df["NormTime"] = normalized_list
    return df



def make_flow_plots(storm_df):

    #start_date = pd.to_datetime('2014-6-26 2:30:00')
    #end_date = pd.to_datetime('2014-6-27 00:7:00')
    start_date = storm_df.iloc[1]["DateTime"]
    end_date = storm_df.iloc[-1]["DateTime"]
    df = normalize_columns(storm_df, start_date, end_date)
    print("flow",df)
    plt.scatter(df.NormTime, df.flow, c=df.solute, cmap='Reds', alpha = 0.75)
    #plt.xlim([pd.to_datetime('2014-6-26 2:30:00'), pd.to_datetime('2014-6-26 00:7:00')])
    #plt.xlim([pd.to_datetime('2014-6-20 2:30:00'), pd.to_datetime('2014-7-26 00:7:00')])


    #plt.clf()
    #plt.scatter(df.flow, df.solute, c=df.NormTime, cmap='Reds')
    #plt.savefig("output2.png")

def make_solute_plots(storm_df):
    start_date = storm_df.iloc[1]["DateTime"]
    print(start_date)
    end_date = storm_df.iloc[-1]["DateTime"]
    df = normalize_columns(storm_df, start_date, end_date)
    print("solute",df)
    plt.scatter(df.flow, df.solute, c=df.NormTime, cmap='Reds', alpha = 0.75)


def make_main_plots(flowcsv, labeled_precip_csv):
    with open(flowcsv) as flow_csv:
      with open(labeled_precip_csv) as precip_csv:
          data = visualizations(flow_csv, precip_csv).reset_index() 

          make_flow_plots(data)

          plt.xlabel("time")
          plt.ylabel("flow rate")
          plt.title("time vs flow rate; normalized solute from light red->dark red")
          plt.savefig("output1000.png")

          plt.clf()


          make_solute_plots(data)


          plt.xlabel("flow rate")
          plt.ylabel("solute")
          plt.title("flow rate vs solute; normalized time from light red->dark red")
          plt.savefig("output1001.png")
