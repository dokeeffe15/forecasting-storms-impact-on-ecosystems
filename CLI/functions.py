import MLfunctions
import pandas as pd
import matplotlib
from matplotlib.axes import Axes
from matplotlib import pyplot as pl
import numpy as np
from typing import List, Tuple
from pathlib import Path
#from scipy.signal import find_peaks
#from scipy.signal import general_gaussian
#from scipy.signal import fftconvolve

import pickle

def read_observs(csv_file):
    d = pd.read_csv(csv_file)
    name = csv_file.name
    site = name[:name.find('_')]
    d = d.dropna(axis='index', how='all')
    d['Site'] = site
    dt = pd.Series(pd.to_datetime(d['Date'] + ' ' + d['Time'],
                                          format='%m/%d/%Y %H:%M'),
                       name='DateTime')
    d = d.join(dt)
    del d['Date']
    return d


def load_all() -> pd.DataFrame:
    ds = []
    for file in Path.cwd().glob('*.csv'):
        try:
            d = read_observs(file)
            ds.append(d)
        except KeyError:
            print("Could not parse fie: " + file.name)
    return pd.concat(ds, sort=False)


def plot_thing(s: pd.DataFrame, thing, mean_interval=None, scaling=None, ax=None):
    thing_to_plot = s[thing].copy().reset_index()
    thing_to_plot = thing_to_plot.set_index('DateTime')

    if scaling is not None:
        thing_to_plot *= scaling

    if mean_interval is not None:
        mean = thing_to_plot.rolling(mean_interval).mean()
        return mean.plot(ax=ax)
    else:
        return thing_to_plot.plot(ax=ax)


def filter_event_by_time(data, event_condition, smallest_time, column):
    # cumsum: each group of trues get their own number, starting from 0
    # apply: each group is transformed into a row containing the starting and ending times

    def first_last(g):
        return g.iloc[0], g.iloc[-1]

    groups = (~event_condition).cumsum(). \
        reset_index().groupby(column)['DateTime']. \
        apply(first_last).apply(pd.Series, index=["first", "last"]). \
        reset_index()

    interval_length = groups['last'] - groups['first']

    long_enough = groups[interval_length > smallest_time].reset_index()

    first = long_enough['first'].rename({'first': 'DateTime'})
    last = long_enough['last'].rename({'last': 'DateTime'})

    selector = data.reset_index()["DateTime"].apply(lambda v: ((v > first) & (v <= last)).any())
    selector = selector.rename(column)
    return data.reset_index()[selector]


def save_is_storm(storm_data, all_data, filename):
    to_save = create_column(storm_data, all_data)
    # print(to_save)
    to_save.to_csv(filename)


def create_column(storm_data, all_data, inverse=False):
    r = storm_data.copy().set_index("DateTime")
    r["is_storm"] = 1 if not inverse else 0
    d = all_data.copy()
    d["is_storm"] = np.nan
    to_save = d.fillna(r)
    to_save["is_storm"] = to_save["is_storm"].fillna(0 if not inverse else 1)
    return to_save


def plot_one_site(data):
    flow_rate = data["Q.cms"]
    flow_rate = get_z_score(flow_rate, flow_rate.mean(), flow_rate.std())
    mean = flow_rate.mean()

    # restricted = filter_event_by_time(flow_rate, flow_rate > mean, '1D', 'Q.cms')
    #
    # # print(mean)
    #
    is_storm = flow_rate[flow_rate > mean]
    # not_storm = flow_rate[flow_rate <= mean]

    complete = create_column(is_storm.reset_index(), data, inverse=True)
    groups = complete["is_storm"].cumsum().reset_index().groupby("is_storm")

    def plot_spans(g):
        g = g.reset_index().set_index("DateTime").index
        start = g[0]
        end = g[-1]
        if len(g) != 1:
            pl.axvspan(start, end, alpha=0.2)
        return g

    groups.apply(plot_spans)
    pl.plot(flow_rate.index, flow_rate)


def predict_one_site(data, column="Q.cms"):

    pd.set_option("display.max_rows", None)

    flow_rate = data[column]

    flow_rate = flow_rate.to_frame()

    flow_rate = flow_rate['Val'].rolling(75,min_periods=1).mean()

    flow_rate = flow_rate[flow_rate > 0]

    return flow_rate


def over_z_score(val, mean_val, std_dev_val, throw_out_z_score):
    z = (val - mean_val)/std_dev_val
    return (z > throw_out_z_score) or (z < -throw_out_z_score)


def get_z_score(val, mean_val, std_dev_val):
    return (val - mean_val)/std_dev_val

def label_dumb_storms(input_csv, output_csv, start, end):
    # Dumb Storm Event Detector

    def with_hour(precip_data, hour):
        hrstr = str(hour) if hour >= 10 else "0" + str(hour)
        index = "HR" + hrstr + "Val"
        t = precip_data[index].reset_index()
        t["DATE"] = t["DATE"] + pd.offsets.Hour(hour)
        return t.rename(columns={index: "Val", "DATE": "DateTime"}).set_index("DateTime")

    def normalize(data, col):
        return (data[col] - data[col].mean()) / data[col].std()


    precip_data = pd.read_csv(input_csv)
    precip_data["DATE"] = pd.to_datetime(precip_data["DATE"])
    precip_data = precip_data.set_index("DATE")

    temp = []
    for hr in range(24):
        temp.append(with_hour(precip_data, hr))
    precip_data = pd.concat(temp).sort_index()

    # %%

    start_date = pd.to_datetime(start)
    end_date = pd.to_datetime(end)

    # filter time
    precip_data = precip_data[(precip_data.index > start_date) & (precip_data.index < end_date)]

    # filter bad data
    precip_data = precip_data[precip_data["Val"] != -9999]
    # print(precip_data)

    axes = normalize(precip_data, "Val").plot()

    # %%

    predicted = predict_one_site(precip_data, "Val")
    save_is_storm(predicted.reset_index(), precip_data, output_csv)

def get_start_date(csv, user_input):
    if user_input is not None:
        return user_input + " 00:00:00"
    else:
        data = read_observs(csv)
        return data.iloc[0]["DateTime"]

def get_end_date(csv, user_input):
    if user_input is not None:
        return user_input + " 00:00:00"
    else:
        data = read_observs(csv)
        return data.iloc[-1]["DateTime"]
