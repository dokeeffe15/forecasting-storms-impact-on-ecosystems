import csv
import matplotlib.pyplot as pld
from datetime import datetime
import pandas as pd
import os
import numpy as np

def over_z_score(val, mean_val, std_dev_val, throw_out_z_score):
    z = (val - mean_val)/std_dev_val
    return (z > throw_out_z_score) or (z < -throw_out_z_score)

def get_z_score(val, mean_val, std_dev_val):
    return (val - mean_val)/std_dev_val

if __name__ == '__main__':
    Z_SCORE_THRESHOLD_THROW_OUT = 4
    x_map = {}#site->dates
    y_map = {}#site->flow values
    strd = os.curdir + "/csv"
    files = os.listdir(strd)

    csv_files = []
    for f in files:
        csv_files.append(f)


    #maybe throw away anything above ~4 z score?
    #step 1: import from csv
    #step 2: calculate mean
    #step 3: calculate z scores and throw away stuff that dont match well
    #calculate mean and std dev again with stuff thrown out
    #step 4: plot
    for csv_name in csv_files:
        with open("./csv/" + csv_name, newline='') as csvfile:
            #GOF = "GOF" in csv_name
            #LMP = "LMP" in csv_name

            print(csv_name)
            reader = csv.DictReader(csvfile)
            x_map[csv_name] = []
            y_map[csv_name] = []
            for row in reader:

                testing = row['Year'] + " " + row['YearDay'] + " " + str(int(float(row['Hour']))) + " " + row['Minute']

                x_map[csv_name].append(datetime.strptime(testing, "%Y %j %H %M"))
                try:
                    #throw out huge values
                    #if float(row['Q']) > 200:
                    #    del x_map[csv_name][-1]
                    #else:
                    y_map[csv_name].append(float(row['Q']))
                except ValueError:
                    del x_map[csv_name][-1]
                except:
                    print("d")

                #print()

    #calculate means & std dev then throw out
    for csv_name in csv_files:
        print(csv_name)
        curr_list = y_map[csv_name]
        std_dev = np.std(curr_list)
        mean = np.mean(curr_list)
        for index, _ in enumerate(y_map[csv_name]):
            if over_z_score(curr_list[index], mean, std_dev, Z_SCORE_THRESHOLD_THROW_OUT):
                del y_map[csv_name][index]
                del x_map[csv_name][index]

    # calculate mean and std dev again because the outliers are thrown out & put in z score vals
    for csv_name in csv_files:
        print(csv_name)
        curr_list = y_map[csv_name]
        std_dev = np.std(curr_list)
        mean = np.mean(curr_list)
        for index, _ in enumerate(y_map[csv_name]):
            #if "WHB" in csv_name and ("2015" in x_map[csv_name][index].strftime("%Y")):
            #    print(x_map[csv_name][index], "z score", get_z_score(y_map[csv_name][index], mean, std_dev), " flow rate:", y_map[csv_name][index])
            y_map[csv_name][index] = get_z_score(y_map[csv_name][index], mean, std_dev)



    (fig, ax) = pld.subplots(1, 1)
    for csv_name in csv_files:
        ax.plot(x_map[csv_name], y_map[csv_name], label=csv_name[0:3])

    ax.legend(loc='upper left')
    ax.set_xlim([pd.to_datetime('2015-5-20 23:50:00'), pd.to_datetime('2015-9-1 00:10:00')])
    ax.set_ylim([-2, 10])
    fig.show()





