# How to use CLI tool
Go into the CLI directory

Run ```python3 main.py -h``` for details on each command
Run ```python3 main.py train -h```, ```python3 main.py test -h```, ```python3 main.py predict -h``` etc for argument list for each command
usage: main.py command args
# train
Performs training on flow data and outputs model in working directory. Arguments are precipitation csv, flow rate csv, and algorithm to use.
# test 
Performs testing on flow data, and outputs a confusion matrix. Arguments are precipitation csv, and flow rate csv.
# predict 
Predicts using the model used in train(uses testing file in directory). Argument is flow rate data and output csv name.

Flow rate data from https://ddc.unh.edu/
Precipitation data from https://www.ncei.noaa.gov/data/coop-hourly-precipitation/v2/


# Example runs
Since the train command saves the model, you will have to run train before running test or predict.

```python3 main.py train -f CHSB_2013_06_to_2015_05_v1.csv -p USC00272174.csv -s 2014-8-30 -e 2014-11-1  -l logreg -m logreg_model.py -q Q.cms```

```python3 main.py test -f CHSB_2013_06_to_2015_05_v1.csv -p USC00272174.csv -s 2014-8-30 -e 2014-11-1 -l logreg -m logreg_model.py -q Q.cms```

```python3 main.py predict -f CHSB_2013_06_to_2015_05_v1.csv -o output.csv -l logreg -m logreg_model.py```

# Visualizations

# visualize
Creates flow vs solute vs time plots, saved as output PNGs. Uses labeled precipitation data for storm data right now. TODO need to use actual ML data and create multiple plots for each storm?

```python3 main.py visualize -f CHSB_2013_06_to_2015_05_v1.csv -l LabeledByPrecipitation.csv```

# classify
Creates flow vs solute vs time plots and classifies them as counterclockwise or clockwise. Outputs PNGs with classification as title

```python3 main.py classify -f CHSB_2013_06_to_2015_05_v1.csv -l LabeledByPrecipitation.csv```


